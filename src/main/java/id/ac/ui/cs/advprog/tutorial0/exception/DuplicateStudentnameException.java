package id.ac.ui.cs.advprog.tutorial0.exception;

public class DuplicateStudentnameException extends RuntimeException{

    public DuplicateStudentnameException(String studentName) {
        super(String.format("The student name %s is a duplicate!", studentName));
    }
}
